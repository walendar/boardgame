

var cvs = document.getElementById("canvas");
var ctx = cvs.getContext("2d");

var buttons = document.getElementById("rules");
buttons.innerHTML = '<img src="tiles/rules.png">';
rules.onclick = function(){
  alert("Правила:\nДобро пожаловать в игру про семью!\nЗарабатывайте деньги и повышайте квалификацию, чтобы зарабатывать больше.\n При измене партнёра вы начинаете всё сначала. Если у вас набирается 3 ссоры вы тоже начинаете сначала. Вы проигрываете, если у вас не остаётся денег.\n От количества детей зависят расходы на них.\n При покупке машины, заплатив за бензин, вы можете продвинуться на 1 клетку вперёд. \n Ваша цель - прожить со своим партнёром до серебряной свадьбы!");
}

let field = new Image();
let p1 = new Image();
let p2 = new Image();
let coubVal;
let x1 = 28, y1 = 796, l1=1, money1=50, job1=0, kids1=0, arg1=0, car1=0;
let x2 = 68, y2 = 836, l2=1, money2=50, job2=0, kids2=0, arg2=0, car2=0;
let turn=1;

field.src = "gamefield.jpg";
p1.src = "women/1.jpg";
p2.src = "women/2.png";

function draw(){
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, 1664, 896);
  ctx.fillStyle = "black";
  ctx.font = '30px Cambria';
  ctx.drawImage(field,0,0);
  ctx.drawImage(p1,x1,y1);
  ctx.drawImage(p2,x2,y2);
  ctx.fillText('Деньги первого игрока: ' + money1, 1300, 300);
  ctx.fillText('Деньги второго игрока: ' + money2, 1300, 500);
  ctx.fillText('Детей: ' + kids1, 1300, 350);
  ctx.fillText('Детей: ' + kids2, 1300, 550);
  ctx.fillText('Ссоры: ' + arg1, 1300, 400);
  ctx.fillText('Ссоры: ' + arg2, 1300, 600);
}
field.onload = draw;
var buttons = document.getElementById("coub");
buttons.innerHTML = '<img src="tiles/coub.png">';
coub.onclick = function() {
  alert(coubVal = Math.floor(Math.random() * (7 - 1)) + 1)
  if(turn%2==0){
    for (let i=0; i<coubVal*128; i+=128)
  {
    switch(true){
      case x2==68+128*9 && l2%2!=0:
      case x2==68 && l2%2==0: {y2-=128; l2++; break;}
      case l2%2!=0: {x2+=128; break;}
      case l2%2==0: {x2-=128; break;}
    }
  }
check(x2,l2);
if (money2<0) {alert("У вас не осталось денег! Первый игрок выигрывает!"); Location.reload();}
draw();
}

  else
  {
  for (let i=0; i<coubVal*128; i+=128)
{
  switch(true){
    case x1==28+128*9 && l1%2!=0:
    case x1==28 && l1%2==0: {y1-=128; l1++; break;}
    case l1%2!=0: {x1+=128; break;}
    case l1%2==0: {x1-=128; break;}
  }
}
check(x1,l1);
if (money1<0) {alert("У вас не осталось денег! Второй игрок выигрывает!"); Location.reload();}
draw();
}
turn++;
};

function check(x, l){
  if (l==1){
    if ((x==28+128) || (x==68+128))
      friends();
    if ((x==28+2*128) || (x==68+2*128))
      argument();
    if ((x==28+3*128) || (x==68+3*128))
      wages();
    if ((x==28+4*128) || (x==68+4*128))
      beautySalon();
    if ((x==28+5*128) || (x==68+5*128))
      wages();
    if ((x==28+6*128) || (x==68+6*128))
      argument();
    if ((x==28+7*128) || (x==68+7*128))
      premiya();
    if ((x==28+8*128) || (x==68+8*128))
      peace();
    if ((x==28+9*128) || (x==68+9*128))
      job();
    }
  if (l==2){
    if ((x==28) || (x==68))
      birth();
    if ((x==28+128) || (x==68+128))
      taxes();
    if ((x==28+2*128) || (x==68+2*128))
      argument();
    if ((x==28+3*128) || (x==68+3*128))
      beautySalon();
    if ((x==28+4*128) || (x==68+4*128))
      peace();
    if ((x==28+5*128) || (x==68+5*128))
      premiya();
    if ((x==28+6*128) || (x==68+6*128))
      argument();
    if ((x==28+7*128) || (x==68+7*128))
      atelye();
    if ((x==28+8*128) || (x==68+8*128))
      wages();
    if ((x==28+9*128) || (x==68+9*128))
      taxes();
    }
  if (l==3){
      if ((x==28) || (x==68))
        premiya();
      if ((x==28+128) || (x==68+128))
        argument();
      if ((x==28+2*128) || (x==68+2*128))
        wages();
      if ((x==28+3*128) || (x==68+3*128))
        peace();
      if ((x==28+4*128) || (x==68+4*128))
        atelye();
      if ((x==28+5*128) || (x==68+5*128))
        friends();
      if ((x==28+6*128) || (x==68+6*128))
        argument();
      if ((x==28+7*128) || (x==68+7*128))
        hospital();
      if ((x==28+8*128) || (x==68+8*128))
        peace();
      if ((x==28+9*128) || (x==68+9*128))
        kids();
      }
  if (l==4){
        if ((x==28) || (x==68))
          kids();
        if ((x==28+128) || (x==68+128))
          premiya();
        if ((x==28+2*128) || (x==68+2*128))
          cheating();
        if ((x==28+3*128) || (x==68+3*128))
          hospital();
        if ((x==28+4*128) || (x==68+4*128))
          argument();
        if ((x==28+5*128) || (x==68+5*128))
          wages();
        if ((x==28+6*128) || (x==68+6*128))
          beautySalon();
        if ((x==28+7*128) || (x==68+7*128))
          argument();
        if ((x==28+8*128) || (x==68+8*128))
          premiya();
        if ((x==28+9*128) || (x==68+9*128))
          job();
        }
  if (l==5){
        if ((x==28) || (x==68))
            car();
        if ((x==28+128) || (x==68+128))
            peace();
        if ((x==28+2*128) || (x==68+2*128))
            atelye();
        if ((x==28+3*128) || (x==68+3*128))
            wages();
        if ((x==28+4*128) || (x==68+4*128))
            argument();
        if ((x==28+5*128) || (x==68+5*128))
            friends();
        if ((x==28+6*128) || (x==68+6*128))
            cheating();
        if ((x==28+7*128) || (x==68+7*128))
            wages();
        if ((x==28+8*128) || (x==68+8*128))
            beautySalon();
        if ((x==28+9*128) || (x==68+9*128))
            kids();
          }
  if (l==6){
            if ((x==28) || (x==68))
              kids();
            if ((x==28+128) || (x==68+128))
              wages();
            if ((x==28+2*128) || (x==68+2*128))
              peace();
            if ((x==28+3*128) || (x==68+3*128))
              taxes();
            if ((x==28+4*128) || (x==68+4*128))
              premiya();
            if ((x==28+5*128) || (x==68+5*128))
              cheating();
            if ((x==28+6*128) || (x==68+6*128))
              beautySalon();
            if ((x==28+7*128) || (x==68+7*128))
              wages();
            if ((x==28+8*128) || (x==68+8*128))
              cheating();
            if ((x==28+9*128) || (x==68+9*128))
              premiya();
            }
  if (l==7){
              if ((x==28) || (x==68))
                win();
              if ((x==28+128) || (x==68+128))
                taxes();
              if ((x==28+2*128) || (x==68+2*128))
                beautySalon();
              if ((x==28+3*128) || (x==68+3*128))
                argument();
              if ((x==28+4*128) || (x==68+4*128))
                wages();
              if ((x==28+5*128) || (x==68+5*128))
                peace();
              if ((x==28+6*128) || (x==68+6*128))
                wages();
              if ((x==28+7*128) || (x==68+7*128))
                premiya();
              if ((x==28+8*128) || (x==68+8*128))
                atelye();
              if ((x==28+9*128) || (x==68+9*128))
                kids();
              }
  }

  function argument(){
    alert("Ссора");
    if(turn%2==0) {arg2++; if (arg2==3)
      {
        alert("У вас 3 ссоры. Придётся заново.");
        x2 = 68; y2 = 836; arg2=0;
      }
    }
    else {arg1++; if (arg1==3)
      {
        alert("У вас 3 ссоры. Придётся заново.");
        x1 = 28; y1 = 796; arg1=0;
      }
    }
  }
  function atelye(){
    alert("Aтелье -15");
    if(turn%2==0) money2-=15;
    else money1-=15;
  }
  function beautySalon(){
    alert("Салон красоты -5");
    if(turn%2==0) money2-=5;
    else money1-=5;
  }
  function birth(){
    alert("Рождение ребёнка");
    if(turn%2==0) kids2++;
    else kids1++;
  }
  function car(){
    alert("Покупка машины");
  }
  function cheating(){
    alert("Измена, вам придётся выходить замуж снова :(");
    if(turn%2==0) {x2 = 68; y2 = 836;}
    else {x1 = 28; y1 = 796;}
  }
  function friends(){
    alert("Встреча с друзьями +15 (подарок от друзей)");
    if(turn%2==0) money2+=15;
    else money1+=15;
  }
  function hospital(){
    alert("Больница -5");
    if(turn%2==0) money2-=5;
    else money1-=5;
  }
  function job(){
    alert("Работа, вы повышаете квалификацию");
    if(turn%2==0) job2++;
    else job1++;
  }
  function kids(){
    alert("Траты на детей -15");
    if(turn%2==0) money2-=kids2*15;
    else money1-=kids1*15;
  }
  function taxes(){
    alert("Нологи -10");
    if(turn%2==0) money2-=10;
    else money1-=10;
  }
  function wages(){
    alert("Зарплата");
    if(turn%2==0) money2+=job2*10+20;
    else money1+=job1*10+20;
  }
  function peace(){
    alert("Примирение");
    if(turn%2==0) arg2=0;
    else arg1=0;
  }
  function premiya(){
    alert("Премия +20");
    if(turn%2==0) money2+=20;
    else money1+=20;
  }
  function win(){
    alert("Поздравляем! Серебранная свадьба!");
    if(turn%2==0) {alert("Второй игрок выиграл!"); Location.reload();}
    else {alert("Первый игрок выиграл!"); Location.reload();}
  }
